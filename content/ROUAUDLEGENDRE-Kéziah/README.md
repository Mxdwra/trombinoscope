---
prenom: Kéziah
nom: Rouaud--Legendre
---

# **Profil**

![Image de Kéziah Rouaud--Legendre][https://cdn.discordapp.com/attachments/846105168563142726/1156963084989112360/Screenshot_20230710_09320711.png?ex=6516e156&is=65158fd6&hm=af376b9e9b318d7b0dd3ea65d0db4481969f8f20d484cc1c5294a0c22aed3c26&]

Bien le bonjour cher concitoyen, je me nomme Rouaud--Legendre Kéziah et j'ai actuellement 19 ans.  
Je suis étudiant dans le lycée technologique de **La Colinière** en première année.  
Je souhaiterais me tourner vers la cyber-defense car je pense que celle-ci est _nécéssaire_ surtout dans notre société ou la numérisation prend de plus en plus de place.  
Cependant j'aime tout autant le web design, simplement car il y a un très grande liberté de mouvement sans aucune limite si ce n'est que notre imagination.

## Contact

![Image de boite Mail](https://cdn-icons-png.flaticon.com/16/1701/1701853.png) **Mail :** <rouaud.keziah@gmail.com>

![Image de téléphone](https://cdn-icons-png.flaticon.com/16/126/126103.png) **Numéro de téléphone :** 06 06 06 06 06

![Image de Linkedin](https://cdn-icons-png.flaticon.com/16/2175/2175195.png) **Linkedin :** [In progress]

# Formations

* **Brevet des collèges** | _Collège La Neustrie_ | 2015-2019
* **Bac général** | _Lycée Jean Perrin_ | 2019-2023
* **BTS SIO** | _Lycée La Coliniere_ | 2023-2025

# Compétences

| Type de compétences |Détails |  
|-|-|  
| Systèmes d'Exploitation | Windows, Ubuntu |  
| Langage        | Python, Javascript, CSS, HTML |  
| Langue             | Français , Anglais |  

# Expériences 

## Informatiques



## Autres

Stage de 3ème dans l'entreprise **Welbond Armature** en 2019
 - Observation du fonctionnement de l'entreprise en _général_
 - Observation des différents postes dans l'entreprise ainsi que leur intérêt
 - Observation des mesures de sécurité prises en cas en cyber-attaque
 - Mise en pratique sur un logiciel de traitement de texte pour simuler le travail d'un comptable

Saison estival 2023 - Patissier
 - Farine 
 - Sucre
 - Sel
 - Guacamole
 - Disque dur 

<!-- Merci de ne pas prendre en compte ces dernières informations non-représentatives de mon dur labeur durant mon été -->