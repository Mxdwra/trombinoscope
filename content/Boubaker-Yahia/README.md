---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
#prenom: Yahia
#nom: Boubaker

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# profil

![photo](https://imageshack.com/i/pniGKWr9p)

Actuellement en première année de BTS informatique au sein du lycée la colinière, je recherche un stage pour appliquer mes compétences acquises durant ma formation et intégrer le monde professionnel. 

## contact 

![icone email](https://imageshack.com/i/pogGME6Vj) [yahiaboubaker216@gmail.com](mailto:yahiaboubaker216@gmail.com)

![icone tel](https://imageshack.com/i/pnQLnx6Zj)   _0623653867_

![Linkedin_logo](https://imagizer.imageshack.com/img922/5950/rCIwcf.jpg) www.linkedin.com/in/yahia-boubaker-b34871234

## formation

2020/2022: Baccalauréat général, spécialités: maths, physique-chimie et science de l'ingénieur 

2023/2024: BTS Services informatique aux organismes, option: Développement

## compétences
| compétences | exemple | 
|:-|:-:|
| outils de code | Pyton, html, css  |
| outils base de données | tableur, gestion de données |
| Langues | Français, Anglais, Arabe |

## expériences 

Scratch: créer et configurer des jeux sur ce site au collège m'a donné un avant-goût sur le développement.

Option SLAM ( solutions logiciels et applications métiers): Mon ouverture sur le développement a commencé avec des petits projets sur python via cette option et j'ai bien aimé ce que je faisais.

Option SISR ( solutions infrastructures systèmes et réseaux): les projets que j'ai pratiqué pour cette option m'ont pas autant plus que le développement et je trouve que ça convient pas à mon profil.

