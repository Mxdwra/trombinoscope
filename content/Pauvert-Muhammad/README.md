---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Muhammad
nom: Pauvert

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil

![](https://cdn.discordapp.com/attachments/750013655994728510/1164575604088242346/PhotoMuhammadPAUVERT.jpg)




Je suis PAUVERT Muhammad, j'ai 18 ans et j'habite à Nantes. Je suis en Bts SIO (Services informatiques aux organisations) au lycée La Colinière. Je suis diplomé d'un baccalauréat téchnologique STI2D option ITEC. Plus tard, j'aimerai choisir la spécialité SISR (Solutions Logicielles et Applications Métier), pour me spécialisé dans le domaine du système et réseaux

## Contact
Adresse mail : pauvertmuhammad@gmail.com

Linkdin :[Muhammad PAUVERT](https://www.linkedin.com/in/muhammad-pauvert-631996280)

# Formation
-Bac Technologique STI2D (Sciences et technologies de l'industrie et du développement durable) - Lycée La Colinière

-BTS SIO (Services informatiques aux organisations) - Lycée La Colinière

# Compétence

| Compétence |   |
|- | :-|
| Langages de Programmation | Python |
| Systèmes d'Exploitation | Windows, Ubuntu, XUbuntu |
| Langues | Français, Anglais


## Section informatique
