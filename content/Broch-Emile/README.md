---
prenom: Emile
nom: Broch
---

# Profil

![Photo](https://framagit.org/EmileBroch/trombinoscope/-/raw/master/public/media/Emile-Broch.jpg)

Je suis Emile Broch actuellement étudiant en BTS INFORMATIQUE(SIO - Services informatiques aux organisations) au Collège-Lycée La Collinière à Nantes, mes contacts, ainsi que d'autre informations me concernant sont disponible ici.

## Contact

- Mail professionnel: <Emile.Broch.mail@gmail.com>
- Numero de telephone : [07 87 11 32 45](0787113245)

# Formations 

- {2020}-{2022} **BAC STI2D SIN** *Lycée Jean-Perrin*
- {2022}-{2023} **BUT MMI** *IUT Laval*
- {2023}-{20XX} **BTS SIO (SLAM/SISR)** *Collège-Lycée La Collinière*

Suite a mes formations réalisées j'envisage une spécialisation de Développement(SLAM - Solutions Logicielles et Applications Métier)  

# Compétences

| Compétences | Détails |
|-------------|---------|
| Langage de programation | Html5, CSS3, PHP, SQL, Javascript, Java |
| Langue | Français, Anglais, Espagnol |
| Logiciels maitrisés | Affinity(Photo;Designer;Publisher), Jmerise, VsCode, PremierPro |

# Expériences

## informatiques

- Réalisation en équipe d'une application web de Quizz Musicale en ligne du nom de Rap Qiz, la création graphique de la direction artistique ma poussée à développer mon sens critique, étant chef de groupe j'ai travaillé mon leadership et evidemment la gestion projet.

- Iniciation au bas du Hardware au sein de l'etreprise Ludwig informatique, pendant durée de deux semaines j'ai grandement compris a comment bien travailler en autonomie, et evidemment comment l'organisation et le gestion de problemes ainsi que la commhnication avec autrui se deroule dans une entreprise locale et à effectif réduit.

- Réalisation en équipe d'une application web fictive via un CMS(Système de Gestion de Contenu) Wordpress grace a cela j'ai pu apprendre a me fixer des objectifs de mannière organisé et une vision globale d’une problématique le projet étant une refonte totale de la categorie "FACTUEL" du site de l'Agence France-Presse.

## Autres

- (En cours)
- (En cours)
